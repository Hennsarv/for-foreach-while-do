﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace ConsoleApp45
{
    static class Program
    {
        public static string Annanimi // lisan siia kommentaari
        {
            get
            {
                Write("Anna nimi:");
                return ReadLine();
            }
        }

        static void Main(string[] args)
        {
            int[] arvud = { 1, 2, 3, 4, 5, 6, 7 };

            WriteLine("\nTavaline for tsükkel\n");
            for (int i = 0; i < arvud.Length; i++) WriteLine(arvud[i]);

            WriteLine("\nWhile tsükkel sama tegemas\n");
            int j = 0;
            while (j < arvud.Length) Console.WriteLine(arvud[j++]);

            WriteLine("\nSama foreach tsükliga\n");
            foreach (var a in arvud) Console.WriteLine(a);

            WriteLine("\nForeach whilega\n");
            var k = arvud.GetEnumerator();
            while (k.MoveNext()) WriteLine(k.Current);

            WriteLine("\nForeach foriga\n");
            for (var l = arvud.GetEnumerator(); l.MoveNext();)
                WriteLine(l.Current);

            WriteLine("\nEdasi while variandid\n");

            WriteLine("\nTavaline while\n");

            string r;
            while ((r = Annanimi) != "")
            {
                WriteLine($"tere {r}!");
            }

            WriteLine("\nWhile foriga\n");

            for (var t = Annanimi; t!=""; t=Annanimi)
            {
                WriteLine($"tere {t}!");
            }

            WriteLine("\nSiin on do-while loogilisem\n");
            do
            {
                if ((r = Annanimi) == "") break;
                WriteLine($"tere {r}!");
            } while (true);

            WriteLine("\nJa do-while foriga (emulation)\n");
            for (var u = "\n"; u!=""; u=Annanimi)
            {
                if(u != "\n") WriteLine($"tere {u}!");
            }

            /*
             * // for tsükli kaks erinevat esitust
for (init(); condition(); iterate()) body();
for (init(); condition(); body()+iterate());

// while tsükkel ja samaväärne for
while(condition()) body();
for(;condition();body())

// foreach tsükkel ja samaväärne for
foreach(t in collection) body(t);
for(k = collection.GetEnumerator();k.Next();body(k.Current));

// do-tsükkel ja samaväärne for
do body(); while(condition());
for(bool b = true;b;condition()) body();

             * */

        }
    }
}
